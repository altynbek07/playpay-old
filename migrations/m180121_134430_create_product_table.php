<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m180121_134430_create_product_table extends Migration {

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'slug' => $this->string(),
            'viewed' => $this->integer()->defaultValue(0),
            'is_active' => $this->integer()->defaultValue(0),
            'is_featured' => $this->integer()->defaultValue(0),
            'is_special' => $this->integer()->defaultValue(0),
            'sort_order' => $this->integer(),
            'category_id' => $this->integer(),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()'),
            'updated_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product');
    }

}
