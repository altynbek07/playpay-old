<?php

use yii\db\Migration;

/**
 * Handles the creation of table `special`.
 */
class m180216_170604_create_special_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('special', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'discount' => $this->integer(),
            'discount_type' => $this->integer(),
            'is_active' => $this->integer()->defaultValue(0),
            'start_date' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()'),
            'end_date' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()'),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()'),
            'updated_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('special');
    }
}
