<?php

use yii\db\Migration;

/**
 * Handles the creation of table `review`.
 */
class m180129_050101_create_review_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('review', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'digiseller_id' => $this->integer(),
            'type' => $this->string(),
            'text' => $this->text(),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('review');
    }
}
