<?php

use yii\db\Migration;

/**
 * Handles the creation of table `digiseller`.
 */
class m180121_135034_create_digiseller_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('digiseller', [
            'id' => $this->primaryKey(),
            'product_id_digiseller' => $this->integer(),
            'product_url' => $this->string(),
            'product_buy_url' => $this->string(),
            'name' => $this->string(),
            'in_stock' => $this->integer(),
            'price_wmr' => $this->integer(),
            'statistic_sales' => $this->integer(),
            'sort_order' => $this->integer(),
            'product_id' => $this->integer(),
            'seller_id_digiseller' => $this->integer(),
            'seller_name_digiseller' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('digiseller');
    }
}
