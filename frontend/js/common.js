$(function () {

    // init youplay
    if (typeof youplay !== 'undefined') {
        youplay.init({
            // enable parallax
            parallax: true,

            // set small navbar on load
            navbarSmall: false,

            // enable fade effect between pages
            fadeBetweenPages: true,

            // twitter and instagram php paths
            php: {
                twitter: './php/twitter/tweet.php',
                instagram: './php/instagram/instagram.php'
            }
        });
    }

    // coutdown
    $(".countdown").each(function () {
        $(this).countdown($(this).attr('data-end'), function (event) {
            $(this).text(
                event.strftime('%D days %H:%M:%S')
            );
        });
    })
});
