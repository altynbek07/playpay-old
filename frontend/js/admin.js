$(function () {
    //image description input
    $("body").on("blur", ".kv-input", function () {
        var input = $(this);
        var id = input.attr("id");
        var description = input.val();
        $.ajax({
            url: '/admin/image/default/edit-description?id=' + id + '&description=' + description,
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jQxhr) {
                if (data.success === true) {
                    $(input).html(data.message);
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });
});
