<?php
/**
 * @var $product app\modules\product\models\Product
 */
?>
<!-- Banner -->
<div class="youplay-banner banner-top youplay-banner-parallax">
    <div class="image" style="background-image: url('/img/demo/game-road-no-taken-1400x788.jpg')">
    </div>

    <div class="info">
        <div>
            <div class="container">
                <h1><?= $product->name ?></h1>
                <br>
                <a href="<?= $product->digiseller->product_buy_url ?>" class="btn btn-lg" title="Купить"><?= $product->price ?> ₸</a>
            </div>
        </div>
    </div>
</div>
<!-- /Banner -->


<!-- Images With Text -->
<div class="youplay-carousel gallery-popup">
    <a class="angled-img" href="http://www.youtube.com/watch?v=_VNV78nLV5Q">
        <div class="img">
            <img src="/img/demo/game-road-no-taken-500x375.jpg" alt="">
        </div>
        <i class="fa fa-play icon"></i>
    </a>
    <a class="angled-img" href="/img/demo/game-road-no-taken-2-1920x1080.jpg">
        <div class="img">
            <img src="/img/demo/game-road-no-taken-2-500x375.jpg" alt="">
        </div>
        <i class="fa fa-search-plus icon"></i>
    </a>
    <a class="angled-img" href="/img/demo/game-road-no-taken-3-1920x1080.jpg">
        <div class="img">
            <img src="/img/demo/game-road-no-taken-3-500x375.jpg" alt="">
        </div>
        <i class="fa fa-search-plus icon"></i>
    </a>
    <a class="angled-img" href="/img/demo/game-road-no-taken-4-1920x1080.jpg">
        <div class="img">
            <img src="/img/demo/game-road-no-taken-4-500x375.jpg" alt="">
        </div>
        <i class="fa fa-search-plus icon"></i>
    </a>
    <a class="angled-img" href="/img/demo/game-road-no-taken-5-1920x1080.jpg">
        <div class="img">
            <img src="/img/demo/game-road-no-taken-5-500x375.jpg" alt="">
        </div>
        <i class="fa fa-search-plus icon"></i>
    </a>
    <a class="angled-img" href="/img/demo/game-road-no-taken-6-1920x1080.jpg">
        <div class="img">
            <img src="/img/demo/game-road-no-taken-6-500x375.jpg" alt="">
        </div>
        <i class="fa fa-search-plus icon"></i>
    </a>
    <a class="angled-img" href="/img/demo/game-road-no-taken-7-1920x1080.jpg">
        <div class="img">
            <img src="/img/demo/game-road-no-taken-7-500x375.jpg" alt="">
        </div>
        <i class="fa fa-search-plus icon"></i>
    </a>
</div>
<!-- /Images With Text -->
<div class="container youplay-store">

    <div class="col-md-9 col-md-push-3">
        <!-- Post Info -->
        <article>

            <!-- Description -->
            <h2 class="mt-0">Описание</h2>
            <div class="description">
                <?= $product->description ?>
            </div>
            <!-- /Description -->

            <!-- Images With Text -->
            <div class="youplay-carousel gallery-popup">
                <a class="angled-img" href="http://www.youtube.com/watch?v=UP_Kwb9bdHk">
                    <div class="img">
                        <img src="/img/demo/game-botanicula-500x375.jpg" alt="">
                    </div>
                    <i class="fa fa-play icon"></i>
                </a>
                <a class="angled-img" href="/img/demo/game-botanicula-2-1920x1080.jpg">
                    <div class="img">
                        <img src="/img/demo/game-botanicula-2-500x375.jpg" alt="">
                    </div>
                    <i class="fa fa-search-plus icon"></i>
                </a>
                <a class="angled-img" href="/img/demo/game-botanicula-3-1920x1080.jpg">
                    <div class="img">
                        <img src="/img/demo/game-botanicula-3-500x375.jpg" alt="">
                    </div>
                    <i class="fa fa-search-plus icon"></i>
                </a>
                <a class="angled-img" href="/img/demo/game-botanicula-4-1920x1080.jpg">
                    <div class="img">
                        <img src="/img/demo/game-botanicula-4-500x375.jpg" alt="">
                    </div>
                    <i class="fa fa-search-plus icon"></i>
                </a>
                <a class="angled-img" href="/img/demo/game-botanicula-5-1920x1080.jpg">
                    <div class="img">
                        <img src="/img/demo/game-botanicula-5-500x375.jpg" alt="">
                    </div>
                    <i class="fa fa-search-plus icon"></i>
                </a>
                <a class="angled-img" href="/img/demo/game-botanicula-6-1920x1080.jpg">
                    <div class="img">
                        <img src="/img/demo/game-botanicula-6-500x375.jpg" alt="">
                    </div>
                    <i class="fa fa-search-plus icon"></i>
                </a>
            </div>
            <!-- /Images With Text -->

            <!-- Post Share -->
            <div class="btn-group social-list social-likes" data-counters="no">
                <span class="btn btn-default facebook" title="Share link on Facebook"></span>
                <span class="btn btn-default twitter" title="Share link on Twitter"></span>
                <span class="btn btn-default plusone" title="Share link on Google+"></span>
                <span class="btn btn-default pinterest" title="Share image on Pinterest" data-media=""></span>
                <span class="btn btn-default vkontakte" title="Share link on VK"></span>
            </div>
            <!-- /Post Share -->
        </article>
        <!-- /Post Info -->

        <!-- Reviews -->
        <div class="reviews-block mb-0">
            <h2>Отзывы
                <small>(<?= $reviewsCount ?>)</small>
            </h2>

            <!-- Reviews List -->
            <ul class="reviews-list">
                <?php foreach ($reviews as $review) : ?>
                <li>
                    <article>
                        <div class="review-cont clearfix">
                            <div class="rating pull-right">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="date"><i class="fa fa-calendar"></i> <?= Yii::$app->formatter->format($review->created_at, 'relativeTime') ?></div>
                            <div class="review-text">
                                <p>
                                    <?= $review->text ?>
                                </p>
                            </div>
                        </div>
                    </article>
                </li>
                <?php endforeach; ?>
            </ul>
            <!-- /Reviews List -->
        </div>
        <!-- /Reviews -->
    </div>

    <!-- Left Side -->
    <div class="col-md-3 col-md-pull-9">

        <!-- Side Search -->
        <div class="side-block ">
            <p>Search by Games:</p>
            <form action="http://html.nkdev.info/youplay/light/search.html">
                <div class="youplay-input">
                    <input type="text" name="search" placeholder="enter search term">
                </div>
            </form>
        </div>
        <!-- /Side Search -->

        <!-- Side Categories -->
        <div class="side-block ">
            <h4 class="block-title">Categories</h4>
            <ul class="block-content">
                <li><a href="#!">All</a>
                </li>
                <li><a href="#!">Action</a>
                </li>
                <li><a href="#!">Adventure</a>
                </li>
                <li><a href="#!">Casual</a>
                </li>
                <li><a href="#!">Indie</a>
                </li>
                <li><a href="#!">Racing</a>
                </li>
                <li><a href="#!">RPG</a>
                </li>
                <li><a href="#!">Simulation</a>
                </li>
                <li><a href="#!">Strategy</a>
                </li>
            </ul>
        </div>
        <!-- /Side Categories -->

        <!-- Side Popular News -->
        <div class="side-block ">
            <h4 class="block-title">Popular Games</h4>
            <div class="block-content p-0">
                <!-- Single News Block -->
                <div class="row youplay-side-news">
                    <div class="col-xs-3 col-md-4">
                        <a href="store-product-1.html" class="angled-img">
                            <div class="img">
                                <img src="/img/demo/game-road-no-taken-500x375.jpg" alt="">
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-9 col-md-8">
                        <h4 class="ellipsis"><a href="store-product-1.html" title="Road Not Taken">Road Not Taken</a>
                        </h4>
                        <span class="price">$50.00</span>
                    </div>
                </div>
                <!-- /Single News Block -->

                <!-- Single News Block -->
                <div class="row youplay-side-news">
                    <div class="col-xs-3 col-md-4">
                        <a href="#!" class="angled-img">
                            <div class="img">
                                <img src="/img/demo/game-botanicula-500x375.jpg" alt="">
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-9 col-md-8">
                        <h4 class="ellipsis"><a href="#!" title="Botanicula">Botanicula</a></h4>
                        <span class="price">$39.99 <sup><del>$49.99</del></sup></span>
                    </div>
                </div>
                <!-- /Single News Block -->

                <!-- Single News Block -->
                <div class="row youplay-side-news">
                    <div class="col-xs-3 col-md-4">
                        <a href="#!" class="angled-img">
                            <div class="img">
                                <img src="/img/demo/game-journey-500x375.jpg" alt="">
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-9 col-md-8">
                        <h4 class="ellipsis"><a href="#!" title="Journey">Journey</a></h4>
                        <span class="price">$20.00</span>
                    </div>
                </div>
                <!-- /Single News Block -->

                <!-- Single News Block -->
                <div class="row youplay-side-news">
                    <div class="col-xs-3 col-md-4">
                        <a href="#!" class="angled-img">
                            <div class="img">
                                <img src="/img/demo/game-world-of-goo-500x375.jpg" alt="">
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-9 col-md-8">
                        <h4 class="ellipsis"><a href="#!" title="World of Goo">World of Goo</a></h4>
                        <span class="price">$10.00</span>
                    </div>
                </div>
                <!-- /Single News Block -->
            </div>
        </div>
        <!-- /Side Popular News -->

    </div>
    <!-- /Left Side -->

</div>

<!-- Related -->
<h2 class="container">Related</h2>
<br>
<div class="youplay-carousel">
    <a class="angled-img" href="#!">
        <div class="img">
            <img src="/img/demo/game-machinarium-500x375.jpg" alt="">
        </div>
        <div class="over-info">
            <div>
                <div>
                    <h4>Machinarium</h4>
                    <div class="rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                    </div>
                </div>
            </div>
        </div>
    </a>
    <a class="angled-img" href="#!">
        <div class="img">
            <img src="/img/demo/game-broken-age-500x375.jpg" alt="">
        </div>
        <div class="over-info">
            <div>
                <div>
                    <h4>Broken Age</h4>
                    <div class="rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                    </div>
                </div>
            </div>
        </div>
    </a>
    <a class="angled-img" href="#!">
        <div class="img">
            <img src="/img/demo/game-dream-500x375.jpg" alt="">
        </div>
        <div class="over-info">
            <div>
                <div>
                    <h4>Dream</h4>
                    <div class="rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                    </div>
                </div>
            </div>
        </div>
    </a>
    <a class="angled-img" href="#!">
        <div class="img">
            <img src="/img/demo/game-world-of-goo-500x375.jpg" alt="">
        </div>
        <div class="over-info">
            <div>
                <div>
                    <h4>World of Goo</h4>
                    <div class="rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>
<!-- /Related -->