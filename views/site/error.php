<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<!-- Main Content -->
<section class="content-wrap full youplay-404">

    <!-- Banner -->
    <div class="youplay-banner banner-top">
        <div class="image" style="background-image: url('/img/demo/game-journey-7-1920x1080.jpg')">
        </div>

        <div class="info">
            <div>
                <div class="container align-center">
                    <h1>404</h1>
                    <h3>Page Not Found ;(</h3>


                    <form action="http://html.nkdev.info/youplay/light/search.html">
                        <div class="youplay-input">
                            <input type="text" name="search" placeholder="Site Search">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Banner -->

</section>
<!-- /Main Content -->
