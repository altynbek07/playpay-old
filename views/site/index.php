<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<!-- Main Content -->
<section class="content-wrap">
    <!-- Banner -->
    <section class="youplay-banner banner-top youplay-banner-parallax">
        <div class="image" style="background-image: url('/img/demo/banner-bg.jpg')">
        </div>

        <div class="info">
            <div>
                <div class="container">
                    <h1>Journey</h1>
                    <em>"Featuring stunning visuals, a Grammy-nominated musical score, and innovative online cooperative
                        gameplay,<br>Journey™ delivers an innovative interactive game experience like no other."</em>
                    <br>
                    <br>
                    <br>
                    <a class="btn btn-lg" href="#!">Purchase</a>
                </div>
            </div>
        </div>
    </section>
    <!-- /Banner -->

    <!-- Images With Text -->
    <div class="youplay-carousel" data-autoplay="5000">
        <a class="angled-img" href="store-product-1.html">
            <div class="img">
                <img src="/img/demo/game-road-no-taken-500x375.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>Road Not Taken</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img">
                <img src="/img/demo/game-botanicula-500x375.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>Botanicula</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img">
                <img src="/img/demo/game-flower-500x375.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>Flower</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img">
                <img src="/img/demo/game-no-mans-sky-500x375.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>No Man's Sky</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img">
                <img src="/img/demo/game-proteus-500x375.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>Proteus</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img">
                <img src="/img/demo/game-brothers-a-tale-of-two-sons-500x375.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>Brothers: A Tale of Two Sons</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <!-- /Images With Text -->


    <!-- Popular -->
    <h2 class="container h1">Popular <a href="#!" class="btn pull-right">See More</a></h2>
    <div class="youplay-carousel">
        <a class="angled-img" href="#!">
            <div class="img">
                <img src="/img/demo/game-machinarium-500x375.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>Machinarium</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img">
                <img src="/img/demo/game-broken-age-500x375.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>Broken Age</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img">
                <img src="/img/demo/game-dream-500x375.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>Dream</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img">
                <img src="/img/demo/game-world-of-goo-500x375.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>World of Goo</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <!-- /Popular -->


    <!-- Specials -->
    <h2 class="container h1">Specials <a href="#!" class="btn pull-right">See More</a></h2>
    <div class="youplay-carousel">
        <a class="angled-img" href="#!">
            <div class="img img-offset">
                <img src="/img/demo/game-botanicula-500x375.jpg" alt="">
                <div class="badge bg-default">
                    -20%
                </div>
            </div>
            <div class="bottom-info">
                <h4>Botanicula</h4>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="price">$39.99 <sup>
                                <del>$49.99</del>
                            </sup>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img img-offset">
                <img src="/img/demo/game-flower-500x375.jpg" alt="">
                <div class="badge bg-default">
                    -25%
                </div>
            </div>
            <div class="bottom-info">
                <h4>Flower</h4>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="price">$26.25 <sup>
                                <del>$35.00</del>
                            </sup>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img img-offset">
                <img src="/img/demo/game-dream-500x375.jpg" alt="">
                <div class="badge bg-default">
                    -30%
                </div>
            </div>
            <div class="bottom-info">
                <h4>Dream</h4>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="price">$34.99 <sup>
                                <del>$49.99</del>
                            </sup>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img img-offset">
                <img src="/img/demo/game-brothers-a-tale-of-two-sons-500x375.jpg" alt="">
                <div class="badge bg-success">
                    -100%
                </div>
            </div>
            <div class="bottom-info">
                <h4>Brothers: A Tale of Two Sons</h4>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="price"><span class="text-success">FREE!</span> <sup>
                                <del>$29.99</del>
                            </sup>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <!-- /Specials -->


    <!-- Preorder -->
    <div class="h2"></div>
    <section class="youplay-banner youplay-banner-parallax small">
        <div class="image" style="background-image: url('/img/demo/banner-broken-age.jpg');">
        </div>

        <div class="info container align-center">
            <div>
                <h2>Broken Age</h2>

                <!-- See countdown init in bottom of the page -->
                <div class="countdown h2" data-end="2017/01/01"></div>

                <br>
                <br>
                <a class="btn btn-lg" href="#!">Pre-Order</a>
            </div>
        </div>
    </section>
    <!-- /Preorder -->


    <!-- Latest News -->
    <h2 class="container h1">Latest News</h2>
    <section class="youplay-news container">
        <!-- Single News Block -->
        <div class="news-one">
            <div class="row vertical-gutter">
                <div class="col-md-4">
                    <a href="blog-post-1.html" class="angled-img">
                        <div class="img">
                            <img src="/img/demo/game-road-no-taken-500x375.jpg" alt="">
                        </div>
                        <div class="youplay-hexagon-rating youplay-hexagon-rating-small" data-max="10"
                             data-back-color="rgba(60,7,50,0.1)" data-front-color="#3C0732" data-size="50"
                             title="6.8 out of 10"><span>6.8</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-8">
                    <div class="clearfix">
                        <h3 class="h2 pull-left m-0"><a href="blog-post-1.html">Road Not Taken - First Try!</a></h3>
                        <span class="date pull-right"><i class="fa fa-calendar"></i> Today</span>
                    </div>
                    <div class="tags">
                        <i class="fa fa-tags"></i> <a href="#">Road Not Taken</a>, <a href="#">first try</a>, <a
                                href="#">newbie game</a>
                    </div>
                    <div class="description">
                        <p>
                            Ut sibi fuerat socius sagittis. Ego intervenerit. Vere quia a te nuper iratus occidit illos
                            undecim annorum puer. Deinde, si hoc forte qui fuit imperavit.
                        </p>
                        <p>
                            Quod satis pecuniae sempiternum. Ut sciat oportet motum. Nunquam invenies eum. Hic de
                            tabula. Lorem ipsum occurrebat pragmaticam semper ut, si quis ita velim tibi bene
                            recognoscere.
                        </p>
                    </div>
                    <a href="blog-post-1.html" class="btn read-more pull-left">Read More</a>
                </div>
            </div>
        </div>
        <!-- /Single News Block -->

        <!-- Single News Block -->
        <div class="news-one">
            <div class="row vertical-gutter">
                <div class="col-md-4">
                    <a href="blog-post-2.html" class="angled-img">
                        <div class="img">
                            <img src="/img/demo/game-botanicula-500x375.jpg" alt="">
                        </div>
                        <div class="youplay-hexagon-rating youplay-hexagon-rating-small" data-max="10"
                             data-back-color="rgba(60,7,50,0.1)" data-front-color="#3C0732" data-size="50"
                             title="9 out of 10"><span>9</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-8">
                    <div class="clearfix">
                        <h3 class="h2 pull-left m-0"><a href="blog-post-2.html">Coming to Youplay - Botanicula</a></h3>
                        <span class="date pull-right"><i class="fa fa-calendar"></i> March 9, 2015</span>
                    </div>
                    <div class="tags">
                        <i class="fa fa-tags"></i> <a href="#">Botanicula</a>, <a href="#">coming soon</a>, <a href="#">first
                            review</a>, <a href="#">sale date</a>
                    </div>
                    <div class="description">
                        Pergo coctione, et ego, et tu oblivisci Pinkman. Obliviscendum hoc unquam factum. Intelligamus
                        hoc in sola SINGULTO multo aliter atque fructuosa negotium structura. Malo B. Option.
                    </div>
                    <a href="blog-post-2.html" class="btn read-more pull-left">Read More</a>
                </div>
            </div>
        </div>
        <!-- /Single News Block -->

        <!-- Single News Block -->
        <div class="news-one">
            <div class="row vertical-gutter">
                <div class="col-md-4">
                    <a href="blog-post-3.html" class="angled-img">
                        <div class="img">
                            <img src="/img/demo/game-journey-500x375.jpg" alt="">
                        </div>
                        <div class="youplay-hexagon-rating youplay-hexagon-rating-small" data-max="10"
                             data-back-color="rgba(60,7,50,0.1)" data-front-color="#3C0732" data-size="50"
                             title="9.3 out of 10"><span>9.3</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-8">
                    <div class="clearfix">
                        <h3 class="h2 pull-left m-0"><a href="blog-post-3.html">Review Journey</a></h3>
                        <span class="date pull-right"><i class="fa fa-calendar"></i> March 1, 2015</span>
                    </div>
                    <div class="tags">
                        <i class="fa fa-tags"></i> <a href="#">Journey</a>, <a href="#">game</a>, <a href="#">review</a>
                    </div>
                    <div class="description">
                        Nonne vides quid sit? Tu es ... Jesse me respice. Tu ... blowfish sunt. A blowfish! Cogitare.
                        Statura pusillus, nec sapientium panem, nec artificum. Sed predators facile prædam blowfish
                        secretum telum non se habet. Non ille? Quid faciam blowfish, Isai.
                        Blowfish quid faciat? In blowfish inflat, purus?
                    </div>
                    <a href="blog-post-3.html" class="btn read-more">Read More</a>
                </div>
            </div>
        </div>
        <!-- /Single News Block -->
    </section>
    <!-- /Latest News -->


    <!-- Partners -->
    <section class="youplay-banner youplay-banner-parallax small mt-80">
        <div class="image" style="background-image: url('/img/demo/banner-bg.jpg');">
        </div>

        <div class="info align-center">
            <div>
                <h2 class="mb-40">Partners</h2>

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="owl-carousel" data-autoplay="6000">
                            <div class="item">
                                <a href="#">
                                    <img src="/img/demo/partner-logo-1.png" alt="">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="/img/demo/partner-logo-2.png" alt="">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="/img/demo/partner-logo-3.png" alt="">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="/img/demo/partner-logo-4.png" alt="">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="/img/demo/partner-logo-5.png" alt="">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="/img/demo/partner-logo-6.png" alt="">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="/img/demo/partner-logo-7.png" alt="">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="/img/demo/partner-logo-8.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Partners -->


    <!-- Features -->
    <h2 class="container h1">Why Buy from Us</h2>
    <section class="youplay-features container">
        <div class="col-md-3 col-sm-6">
            <a class="feature angled-bg" href="#">
                <i class="fa fa-cc-visa"></i>
                <h3>Payment</h3>
                <small>More than 10 payment systems</small>
            </a>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="feature angled-bg">
                <i class="fa fa-gamepad"></i>
                <h3>Games</h3>
                <small>A large number of games</small>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="feature angled-bg">
                <i class="fa fa-money"></i>
                <h3>Cheap</h3>
                <small>Lowest prices on the Internet</small>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="feature angled-bg">
                <i class="fa fa-users"></i>
                <h3>Community</h3>
                <small>The largest gaming community</small>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="youplay-footer-parallax">
        <div class="wrapper" style="background-image: url('/img/demo/footer-bg.jpg')">

            <!-- Social Buttons -->
            <div class="social">
                <div class="container">
                    <h3>Connect socially with <strong>youplay</strong></h3>

                    <div class="social-icons">
                        <div class="social-icon">
                            <a href="#!">
                                <i class="fa fa-facebook-square"></i>
                                <span>Like on Facebook</span>
                            </a>
                        </div>
                        <div class="social-icon">
                            <a href="#!">
                                <i class="fa fa-twitter-square"></i>
                                <span>Follow on Twitter</span>
                            </a>
                        </div>
                        <div class="social-icon">
                            <a href="#!">
                                <i class="fa fa-twitch"></i>
                                <span>Watch on Twitch</span>
                            </a>
                        </div>
                        <div class="social-icon">
                            <a href="#!">
                                <i class="fa fa-youtube-square"></i>
                                <span>Watch on Youtube</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Social Buttons -->

            <!-- Copyright -->
            <div class="copyright">
                <div class="container">
                    <strong>nK</strong> &copy; 2016. All rights reserved
                </div>
            </div>
            <!-- /Copyright -->

        </div>
    </footer>
    <!-- /Footer -->

</section>
<!-- /Main Content -->