<?php

$params = require __DIR__ . '/params.php';

$db = [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2bridge',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
        // Schema cache options (for production environment)
        //'enableSchemaCache' => true,
        //'schemaCacheDuration' => 60,
        //'schemaCache' => 'cache',
];

return [
    'yiiDebug' => false,
    'yiiEnv' => 'prod',
    'yiiPath' => dirname(__DIR__) . '/vendor/yiisoft/yii2/Yii.php',
    'web' => [
        'id' => 'bridge',
        'language' => 'ru',
//        'timeZone' => 'Asia/Almaty',
        'basePath' => dirname(__DIR__),
        'bootstrap' => [
            'log', 'admin',
            '\app\events\Bootstrap'
        ],
        'aliases' => [
            '@bower' => '@vendor/bower-asset',
            '@npm' => '@vendor/npm-asset',
        ],
        'components' => [
            'request' => [
                // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
                'cookieValidationKey' => '1AD948uWLSSg14DNbb-_x257YmzAydFi',
            ],
            'cache' => [
                'class' => 'yii\caching\FileCache',
            ],
            'user' => [
                'identityClass' => \app\models\User::class,
                'enableAutoLogin' => true,
            ],
            'authManager' => [
                'class' => \Da\User\Component\AuthDbManagerComponent::class,
            ],
            'errorHandler' => [
                'errorAction' => 'site/error',
            ],
            'mailer' => [
                'class' => 'yii\swiftmailer\Mailer',
                // send all mails to a file by default. You have to set
                // 'useFileTransport' to false and configure a transport
                // for the mailer to send real emails.
                'useFileTransport' => true,
            ],
            'log' => [
                'traceLevel' => 3,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'db' => $db,
            'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                'rules' => [
                    '<slug>' => 'product/view',
                ],
            ],
        ],
        'modules' => [
            'admin' => [
                'class' => \naffiq\bridge\BridgeModule::class,
                'userClass' => \app\models\User::class,
                'userSettings' => [
                    'class' => \Da\User\Module::className(),
                    'administratorPermissionName' => 'admin'
                ],
                'extraJs' => [
                    '/js/admin.min.js'
                ],
                'modules' => [
                    'product' => [
                        'class' => 'app\modules\product\Module',
                    ],
                    'image' => [
                        'class' => 'app\modules\image\Module',
                    ],
                    'digiseller' => [
                        'class' => 'app\modules\digiseller\Module',
                    ],
                    'special' => [
                        'class' => 'app\modules\special\Module',
                    ],
                ],
                'menu' => [
                    'Каталог',
                    [
                        'title' => 'Товары',
                        'url' => ['/admin/product/product/index'],
                        'active' => ['module' => 'product', 'controller' => 'product'],
                        'icon' => 'cube'
                    ],
                    [
                        'title' => 'Категории',
                        'url' => ['/admin/product/category/index'],
                        'active' => ['module' => 'product', 'controller' => 'category'],
                        'icon' => 'list'
                    ],
                    [
                        'title' => 'Акции',
                        'url' => ['/admin/special/special/index'],
                        'active' => ['module' => 'special', 'controller' => 'special'],
                        'icon' => 'percent'
                    ],
                    'Digiseller',
                    [
                        'title' => 'Digiseller товары',
                        'url' => ['/admin/digiseller/digiseller/index'],
                        'active' => ['module' => 'digiseller', 'controller' => 'digiseller'],
                        'icon' => 'gamepad'
                    ],
                    [
                        'title' => 'Отзывы',
                        'url' => ['/admin/digiseller/review/index'],
                        'active' => ['module' => 'digiseller', 'controller' => 'review'],
                        'icon' => 'comments'
                    ],
                ],
            ],
        ],
        'params' => $params,
    ],
    'console' => [
        'id' => 'basic-console',
        'basePath' => dirname(__DIR__),
        'bootstrap' => [
            'log', 'admin',
            '\app\events\Bootstrap'
        ],
        'controllerNamespace' => 'app\commands',
        'components' => [
            'cache' => [
                'class' => 'yii\caching\FileCache',
            ],
            'log' => [
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'db' => $db,
        ],
        'modules' => [
            'admin' => [
                'class' => \naffiq\bridge\BridgeModule::class
            ]
        ],
        'params' => $params,
    /*
      'controllerMap' => [
      'fixture' => [ // Fixture generation command line.
      'class' => 'yii\faker\FixtureController',
      ],
      ],
     */
    ]
];
