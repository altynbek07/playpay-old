<?php
/**
 * Created by PhpStorm.
 * User: Developer
 * Date: 18.02.2018
 * Time: 22:54
 */

namespace app\controllers;

use yii\web\Controller;
use app\modules\product\models\Product;
use yii\web\NotFoundHttpException;
use Yii;

class ProductController extends Controller
{
    public function actionView($slug)
    {
        $product = Product::findOne(['slug' => $slug]);

        if (!$product) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $reviews = $product->reviews;
        $reviewsCount = $product->reviewsCount;

        Yii::$app->view->title = $product->name;

        Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => 'Description set inside controller',
        ]);
        Yii::$app->view->registerMetaTag([
            'name' => 'keywords',
            'content' => 'Keywords set inside controller',
        ]);

        return $this->render('view', [
            'product' => $product,
            'reviews' => $reviews,
            'reviewsCount' => $reviewsCount
        ]);
    }
}