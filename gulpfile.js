var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    cleanCSS = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache'),
    autoprefixer = require('gulp-autoprefixer'),
    ftp = require('vinyl-ftp'),
    notify = require("gulp-notify"),
    rsync = require('gulp-rsync');

// Пользовательские скрипты проекта

gulp.task('common-js', function () {
    return gulp.src([
        'frontend/js/common.js',
    ])
        .pipe(concat('common.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('frontend/js'));
});

gulp.task('admin-js', function () {
    return gulp.src([
        'frontend/js/admin.js',
    ])
        .pipe(concat('admin.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('frontend/js'));
});

gulp.task('js', ['common-js', 'admin-js'], function () {
    return gulp.src([
        'frontend/libs/jquery/dist/jquery.min.js',
        'frontend/libs/HexagonProgress/jquery.hexagonprogress.min.js',
        'frontend/libs/bootstrap/dist/js/bootstrap.min.js',
        'frontend/libs/jarallax/dist/jarallax.min.js',
        'frontend/libs/smoothscroll-for-websites/SmoothScroll.js',
        'frontend/libs/owl.carousel/dist/owl.carousel.min.js',
        'frontend/libs/jquery.countdown/dist/jquery.countdown.min.js',
        'frontend/libs/magnific-popup/dist/jquery.magnific-popup.min.js',
        // 'frontend/libs/slider-revolution/rs-plugin/js/jquery.themepunch.tools.min.js',
        // 'frontend/libs/slider-revolution/rs-plugin/js/jquery.themepunch.revolution.min.js',
        'frontend/libs/social-likes/dist/social-likes.min.js',
        'frontend/libs/youplay/js/youplay.min.js',
        'frontend/js/common.min.js', // Всегда в конце
    ])
        .pipe(concat('scripts.min.js'))
        // .pipe(uglify()) // Минимизировать весь js (на выбор)
        .pipe(gulp.dest('frontend/js'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: 'frontend'
        },
        notify: false,
        // tunnel: true,
        // tunnel: "projectmane", //Demonstration page: http://projectmane.localtunnel.me
    });
});

gulp.task('sass', function () {
    return gulp.src('frontend/sass/**/*.sass')
        .pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
        .pipe(rename({suffix: '.min', prefix: ''}))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(cleanCSS()) // Опционально, закомментировать при отладке
        .pipe(gulp.dest('frontend/css'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('watch', ['sass', 'js', 'browser-sync'], function () {
    gulp.watch('frontend/sass/**/*.sass', ['sass']);
    gulp.watch(['libs/**/*.js', 'frontend/js/common.js'], ['js']);
    gulp.watch('frontend/*.html', browserSync.reload);
});

gulp.task('imagemin', function () {
    return gulp.src('frontend/img/**/*')
        .pipe(cache(imagemin())) // Cache Images
        .pipe(gulp.dest('web/img'));
});

gulp.task('build', ['removedir', 'imagemin', 'sass', 'js'], function () {

    /*var buildFiles = gulp.src([
        'frontend/!*.html',
        'frontend/.htaccess',
        ]).pipe(gulp.dest('web'));*/

    var buildCss = gulp.src([
        'frontend/css/main.min.css',
    ]).pipe(gulp.dest('web/css'));

    var buildJs = gulp.src([
        'frontend/js/scripts.min.js',
        'frontend/js/admin.min.js',
    ]).pipe(gulp.dest('web/js'));

    var buildFonts = gulp.src([
        'frontend/fonts/**/*',
    ]).pipe(gulp.dest('web/fonts'));

});

gulp.task('deploy', function () {

    var conn = ftp.create({
        host: 'hostname.com',
        user: 'username',
        password: 'userpassword',
        parallel: 10,
        log: gutil.log
    });

    var globs = [
        'web/**',
        'web/.htaccess',
    ];
    return gulp.src(globs, {buffer: false})
        .pipe(conn.dest('/path/to/folder/on/server'));

});

gulp.task('rsync', function () {
    return gulp.src('web/**')
        .pipe(rsync({
            root: 'web/',
            hostname: 'username@yousite.com',
            destination: 'yousite/public_html/',
            // include: ['*.htaccess'], // Скрытые файлы, которые необходимо включить в деплой
            recursive: true,
            archive: true,
            silent: false,
            compress: true
        }));
});

gulp.task('removedir', function () {
    return del.sync([
        'web/css/*',
        'web/fonts/*',
        'web/img/*',
        'web/js/*',
        'web/*.html'
    ]);
});
gulp.task('clearcache', function () {
    return cache.clearAll();
});

gulp.task('default', ['watch']);
