<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\components\DigisellerApi;
use app\modules\digiseller\models\Digiseller;
use app\modules\digiseller\models\Review;
use yii\helpers\Console;

class DigisellerController extends Controller
{
    /*
     * Обновление информации всех Digiseller товаров
     */
    public function actionUpdateInfo()
    {
        $productDigisellers = Digiseller::find()->all();

        if (!$productDigisellers) {
            Console::output(Console::renderColoredString('%rDigiseller товары не найдены!%n'));
            return false;
        } else {
            foreach ($productDigisellers as $productDigiseller) {
                $result = DigisellerApi::getProductInfo($productDigiseller->product_id_digiseller);

                $productDigiseller->in_stock = $result->product->in_stock;
                $productDigiseller->price_wmr = $result->product->prices->wmr;
                $productDigiseller->statistic_sales = $result->product->statistics->sales;
                $productDigiseller->save(false);
                Console::output(Console::renderColoredString("%gDigiseller товар '{$productDigiseller->name}' был обновлен!%n"));
            }

            Console::output(Console::renderColoredString("%gВсе Digiseller товары были обновлены!%n"));

            return true;
        }
    }

    /*
     * Обновление отзывов всех Digiseller товаров
     */
    public function actionUpdateReviews()
    {
        $productDigisellers = Digiseller::find()->all();

        if (!$productDigisellers) {
            Console::output(Console::renderColoredString('%rDigiseller товары не найдены!%n'));
            return false;
        } else {
            foreach ($productDigisellers as $productDigiseller) {
                Review::deleteAll(['digiseller_id' => $productDigiseller->id]);

                $model = new Review();
                $model->setProductReviews($productDigiseller->id, $productDigiseller->product_id_digiseller, $productDigiseller->seller_id_digiseller);

                Console::output(Console::renderColoredString("%gОтзывы Digiseller товара '{$productDigiseller->name}' были обновлены!%n"));
            }

            Console::output(Console::renderColoredString("%gВсе отзывы Digiseller товаров были обновлены!%n"));

            return true;
        }
    }
}
