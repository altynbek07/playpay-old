<?php

namespace app\components;

class DigisellerApi {

    public static function getProductInfo($productDigisellerId)
    {
        $curl = curl_init("http://shop.digiseller.ru/xml/shop_product_info.asp");

        $xml = "<digiseller.request>
                    <product>
                      <id>{$productDigisellerId}</id>
                    </product>
                </digiseller.request>";

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result = simplexml_load_string($result);
    }

    public static function getProductReviews($productDigisellerId, $productSellerId)
    {
        $curl = curl_init("http://shop.digiseller.ru/xml/shop_reviews.asp");

        $xml = "<digiseller.request>
                    <seller>
                            <id>{$productSellerId}</id>
                    </seller>
                    <product>
                            <id>{$productDigisellerId}</id>
                    </product>
                    <reviews>
                            <type>good</type>
                    </reviews>
                    <pages>
                            <num></num>
                            <rows>50</rows>
                    </pages>
                </digiseller.request>";

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        $reviews = simplexml_load_string($result);
        
        return $reviews->reviews;
    }

}
