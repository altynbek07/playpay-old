<?php

namespace app\modules\product\models;

use app\modules\digiseller\models\Digiseller;
use app\modules\special\models\Special;
use app\modules\image\models\Image;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use app\modules\product\models\query\ProductQuery;
use app\modules\digiseller\models\Review;
use naffiq\tenge\CurrencyRates;
use yii\web\UploadedFile;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $slug
 * @property integer $viewed
 * @property integer $is_active
 * @property integer $is_featured
 * @property integer $is_special
 * @property integer $sort_order
 * @property integer $category_id
 * @property string $created_at
 * @property string $updated_at
 */
class Product extends \yii\db\ActiveRecord
{

    const DIGISELLER_PRODUCT_IN_STOCK = 1;

    public $files;
    public $filesDescriptions;
    public $enableWatermark;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'slug'], 'string'],
            [['viewed', 'is_active', 'sort_order', 'category_id', 'enableWatermark'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'on' => ['create', 'update'], 'max' => 255],
            [['sort_order'], 'default', 'value' => 0],
            [['files'], 'file', 'extensions' => ['gif', 'jpg', 'png', 'jpeg'], 'maxFiles' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'slug' => 'Slug',
            'viewed' => 'Viewed',
            'is_active' => 'Is Active',
            'sort_order' => 'Sort Order',
            'category_id' => 'Category ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getSpecial()
    {
        return $this->hasOne(Special::className(), ['product_id' => 'id']);
    }

    public function getDigiseller()
    {
        return Digiseller::find()->where(['product_id' => $this->id])->andWhere(['in_stock' => self::DIGISELLER_PRODUCT_IN_STOCK])->orderBy('price_wmr ASC')->one();
    }

    public function getReviews()
    {
        return Review::find()->where(['digiseller_id' => $this->digiseller->id])->limit(20)->all();
    }

    public function getReviewsCount()
    {
        return Review::find()->where(['digiseller_id' => $this->digiseller->id])->count();
    }

    public function getPrice()
    {
        $price = $this->digiseller->price_wmr;
        $rates = new CurrencyRates();
        $price_tng = $rates->convertToTenge('RUB', $price);
        return $price_tng;
    }

    public function getSpecialPrice()
    {
        $oldPrice = (int) $this->price;
        $specialDiscount = (int) $this->special->discount;
        $specialDiscountType = (int) $this->special->discount_type;
        $newPrice = Special::applyDiscount($oldPrice, $specialDiscount, $specialDiscountType);
        return $newPrice;
    }

    public function getImages()
    {
        return $this->hasMany(Image::className(), ['model_id' => 'id'])
            ->andWhere(['model_name' => self::className()])
            ->orderBy('sort_order')->all();
    }

    public function getListImageItems($imageData)
    {
        if ($imageData !== 'images' && $imageData !== 'data' && $imageData !== 'description') {
            return false;
        }

        $images = $this->images;

        if (!$images) {
            return false;
        }

        $result = [];

        foreach ($images as $image) {
            if ($imageData === 'images') {
                $result[] = $image->imageUrl;
            } elseif ($imageData === 'data') {
                $result[] = [
                    'caption' => $image->name,
                    'key' => $image->id,
                ];
            } else {
                $result[] = [
                    '{description}' => $image->description ? $image->description : '',
                    '{id}' => $image->id,
                    '{TAG_CSS_INIT}' => ''
                ];
            }
        }

        return $result;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        var_dump($this->enableWatermark);die;
        $files = UploadedFile::getInstances(new Product(), 'files');
        if (!empty($files)) {
            foreach ($files as $file) {
                $filename = Image::uploadFile($file, self::tableName(), $this->id);
                Image::saveFile($filename, self::className(), $this->id);
            }
        }
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general. 
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('CURRENT_TIMESTAMP()'),
            ]
        ];
    }

}
