<?php

namespace app\modules\product\models;

use Yii;
use app\modules\product\models\query\CategoryQuery;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $slug
 * @property integer $sort_order
 */
class Category extends \yii\db\ActiveRecord
{
    const MAIN_PARENT_CATEGORY = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'sort_order'], 'integer'],
            [['name'], 'required'],
            [['name', 'slug'], 'string', 'on' => ['create', 'update'], 'max' => 255],
            [['sort_order', 'parent_id'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'sort_order' => 'Sort Order',
        ];
    }
    
    /**
     * @inheritdoc
     * @return CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }
    
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }
    
    public function getParentCategory()
    {
        return self::find(['parent_id' => $this->parent_id]);
    }
    
    public function getCategoryFullname()
    {
        $categoryFullname = ($this->parent_id === self::MAIN_PARENT_CATEGORY) ? $this->name : $this->parentCategory->name . ' -> ' . $this->name;
        return $categoryFullname;
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],
        ];
    }

}
