<?php

namespace app\modules\product\controllers;

use naffiq\bridge\controllers\BaseAdminController;

/**
 * CategoryController implements the CRUD actions for [[app\modules\product\models\Category]] model.
 * @see app\modules\product\models\Category
 */
class CategoryController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\product\models\Category';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\product\models\search\CategorySearch';


    /**
     * @inheritdoc
     */
    public $createScenario = 'create';

    /**
     * @inheritdoc
     */
    public $updateScenario = 'update';

}
