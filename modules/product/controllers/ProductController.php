<?php

namespace app\modules\product\controllers;

use naffiq\bridge\controllers\BaseAdminController;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * ProductController implements the CRUD actions for [[app\modules\product\models\Product]] model.
 * @see app\modules\product\models\Product
 */
class ProductController extends BaseAdminController {

    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\product\models\Product';

    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\product\models\search\ProductSearch';

    /**
     * @inheritdoc
     */
    public $createScenario = 'create';

    /**
     * @inheritdoc
     */
    public $updateScenario = 'update';

}
