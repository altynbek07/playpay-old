<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];

?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'description:ntext',
                'slug',
                'viewed',
                'is_active',
                'sort_order',
                'category_id',
                [
                    'label' => 'Digiseller name',
                    'value' => $model->digiseller->name,
                ],
                [
                    'label' => 'Price',
                    'value' => function($model) {
                        return $model->is_special ? $model->specialPrice : $model->price;
                    }
                ],
                'created_at',
                'updated_at',
            ],
        ])
        ?>
    </div>
    
    
</div>