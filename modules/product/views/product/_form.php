<?php

use yii\bootstrap\Html;
use naffiq\bridge\widgets\ActiveForm;
use kartik\widgets\FileInput;
use app\modules\product\models\Category;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\Product */
/* @var $form naffiq\bridge\widgets\ActiveForm */
//$this->registerJsFile('/js/custom.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>

<div class="row">
    <div class="col-lg-12">

        <?php
        $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data']
        ]);
        ?>

        <?= $form->field($model, 'enableWatermark')->switchInput() ?>

        <?=
        $form->field($model, 'files[]')->widget(FileInput::className(), [
            'options' => [
                'accept' => 'image/*',
                'multiple' => true,
            ],
            'pluginOptions' => [
                'maxFileCount' => 5,
                'showRemove' => false,
                'showUpload' => false,
                'deleteUrl' => Url::to(['/admin/image/default/delete-image']),
                'initialPreview' => $model->getListImageItems('images'),
                'initialPreviewAsData' => true,
                'overwriteInitial' => false,
                'initialPreviewConfig' => $model->getListImageItems('data'),
                'previewThumbTags' => [
                    '{TAG_CSS_INIT}' => 'kv-hidden'
                ],
                'initialPreviewThumbTags' => $model->getListImageItems('description'),
                'layoutTemplates' => [
                    'footer' => '<div class="file-thumbnail-footer">'
                        . '<div class="file-footer-caption" title="{caption}">'
                        . '<div class="file-caption-info">{caption}</div>'
                        . '<div class="file-size-info">{size}</div>'
                        . '<input id="{id}" class="kv-input kv-init form-control input-sm form-control-sm text-center {TAG_CSS_INIT}" value="{description}" placeholder="Enter caption...">'
                        . '</div>'
                        . '{progress}{indicator}{actions}'
                        . '</div>'
                ],
                'browseOnZoneClick' => true,
            ],
            'pluginEvents' => [
                'filesorted' => new JsExpression('function(event, params){
                    $.post("' . Url::to(["/admin/image/default/sort-image", "modelId" => $model->id, "className" => $model->className()]) . '",{sort_order: params});
                }')
            ],
        ])
        ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->richTextArea(['options' => ['rows' => 6]]) ?>

        <?= $form->field($model, 'is_active')->switchInput() ?>

        <?= $form->field($model, 'sort_order')->textInput() ?>

        <?= $form->field($model, 'category_id')->relationalDropDown(Category::className(), 'id', 'categoryFullname', ['options' => ['placeholder' => 'Select category']]) ?>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>