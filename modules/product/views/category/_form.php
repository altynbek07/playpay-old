<?php

use yii\bootstrap\Html;
use naffiq\bridge\widgets\ActiveForm;
use app\modules\product\models\Category;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\Category */
/* @var $form naffiq\bridge\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-lg-5">

        <?php $form = ActiveForm::begin(); ?>

        <?=
        $form->field($model, 'parent_id')->relationalDropDown(Category::className(), 'id', 'name', [
            'options' => [
                'placeholder' => 'Select digiseller product',
                'options' => [
                    $model->id => ['disabled' => true],
                ]
            ]
        ])
        ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'sort_order')->textInput() ?>

        <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

<?php ActiveForm::end(); ?>

    </div>
</div>