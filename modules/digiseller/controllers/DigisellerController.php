<?php

namespace app\modules\digiseller\controllers;

use naffiq\bridge\controllers\BaseAdminController;

/**
 * DigisellerController implements the CRUD actions for [[app\modules\digiseller\models\Digiseller]] model.
 * @see app\modules\digiseller\models\Digiseller
 */
class DigisellerController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\digiseller\models\Digiseller';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\digiseller\models\search\DigisellerSearch';


    /**
     * @inheritdoc
     */
    public $createScenario = 'create';

    /**
     * @inheritdoc
     */
    public $updateScenario = 'update';

}
