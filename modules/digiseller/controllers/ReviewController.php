<?php

namespace app\modules\digiseller\controllers;

use naffiq\bridge\controllers\BaseAdminController;

/**
 * ReviewController implements the CRUD actions for [[app\modules\digiseller\models\Review]] model.
 * @see app\modules\digiseller\models\Review
 */
class ReviewController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\digiseller\models\Review';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\digiseller\models\search\ReviewSearch';


    /**
     * @inheritdoc
     */
    public $createScenario = 'create';

    /**
     * @inheritdoc
     */
    public $updateScenario = 'update';

}
