<?php

use yii\grid\GridView;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\digiseller\models\DigisellerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Digisellers';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create']
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

//        'id',
        'product_id_digiseller',
//        'seller_id_digiseller',
//        'product_url:url',
//        'product_buy_url:url',
         'name',
         'in_stock',
        // 'price_wmr',
        // 'statistic_sales',
        // 'sort_order',
        // 'product_id',
        // 'created_at',
        // 'updated_at',

        [
            'class' => ActionColumn::className(),
        ],
    ],
]); ?>
