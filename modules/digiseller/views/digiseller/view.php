<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\digiseller\models\Digiseller */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Digisellers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'product_id_digiseller',
            'seller_id_digiseller',
            'product_url:url',
            'product_buy_url:url',
            'name',
            'in_stock',
            'price_wmr',
            'statistic_sales',
            'sort_order',
            'product_id',
            'created_at',
            'updated_at',
        ],
    ]) ?>
    </div>
</div>