<?php

use yii\bootstrap\Html;
use naffiq\bridge\widgets\ActiveForm;
use app\modules\product\models\Product;

/* @var $this yii\web\View */
/* @var $model app\modules\digiseller\models\Digiseller */
/* @var $form naffiq\bridge\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-lg-5">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'product_url')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'product_buy_url')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'sort_order')->textInput() ?>

        <?= $form->field($model, 'product_id')->relationalDropDown(Product::className(), 'id', 'name', ['options' => ['placeholder' => 'Select product']]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>