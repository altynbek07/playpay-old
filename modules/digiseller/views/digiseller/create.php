<?php

/* @var $this yii\web\View */
/* @var $model app\modules\digiseller\models\Digiseller */

$this->title = 'Create Digiseller';
$this->params['breadcrumbs'][] = ['label' => 'Digisellers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

