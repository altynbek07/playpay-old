<?php

namespace app\modules\digiseller\models;

use Yii;
use app\components\DigisellerApi;

/**
 * This is the model class for table "review".
 *
 * @property integer $id
 * @property integer $digiseller_id
 * @property string $type
 * @property string $text
 * @property string $created_at
 */
class Review extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['digiseller_id'], 'integer'],
            [['text'], 'string'],
            [['created_at'], 'safe'],
            [['type'], 'string', 'on' => ['create', 'update'], 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'digiseller_id' => 'Digiseller ID',
            'type' => 'Type',
            'text' => 'Text',
            'created_at' => 'Created At',
        ];
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\digiseller\models\query\ReviewQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\digiseller\models\query\ReviewQuery(get_called_class());
    }
    
    /*
     * Сохраняем отзывы товара
     * @param $productDigisellerId integer
     * @param $productSellerId integer
     * @param $productId integer
     */
    public function setProductReviews($digisellerId, $productDigisellerId, $productSellerId)
    {
        $reviews = DigisellerApi::getProductReviews($productDigisellerId, $productSellerId);

        /*
         * Проверка на количество отзывов
         * Если переменная $reviews['cnt'] не пуста, то есть количество больше 0, то добавляем отзывы
         * @var $reviews['cnt'] string
         */
        if (!empty($reviews['cnt'])) {
            foreach ($reviews->review as $review) {
                $data[] = [
                    'digiseller_id' => $digisellerId,
                    'type' => (string) $review->type,
                    'text' => (string) $review->info,
                    // Конвертируем дату в формат 2018-01-28 10:25:25. Изначально дата приходит в таком виде 28.01.2018 10:25:25
                    'created_at' => Yii::$app->formatter->asDatetime((string) $review->date, 'yyyy-MM-dd H:m:s'),
                ];
            }

            Yii::$app->db->createCommand()->batchInsert('review', [
                'digiseller_id',
                'type',
                'text',
                'created_at'
                    ], $data)->execute();
        }
    }
    

}
