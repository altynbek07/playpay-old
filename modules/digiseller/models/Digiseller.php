<?php

namespace app\modules\digiseller\models;

use Yii;
use app\components\DigisellerApi;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use app\modules\digiseller\models\query\DigisellerQuery;

/**
 * This is the model class for table "digiseller".
 *
 * @property integer $id
 * @property integer $product_id_digiseller
 * @property integer $seller_id_digiseller
 * @property integer $seller_name_digiseller
 * @property string $product_url
 * @property string $product_buy_url
 * @property string $name
 * @property integer $in_stock
 * @property integer $price_wmr
 * @property integer $statistic_sales
 * @property integer $sort_order
 * @property integer $product_id
 * @property string $created_at
 * @property string $updated_at
 */
class Digiseller extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'digiseller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id_digiseller', 'seller_id_digiseller', 'in_stock', 'price_wmr', 'statistic_sales', 'sort_order', 'product_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['product_url', 'product_buy_url', 'name', 'seller_name_digiseller'], 'string', 'on' => ['create', 'update'], 'max' => 255],
            [['sort_order'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id_digiseller' => 'Product Id Digiseller',
            'seller_id_digiseller' => 'Seller Id Digiseller',
            'seller_name_digiseller' => 'Seller Name Digiseller',
            'product_url' => 'Product Url',
            'product_buy_url' => 'Product Buy Url',
            'name' => 'Name',
            'in_stock' => 'In Stock',
            'price_wmr' => 'Price Wmr',
            'statistic_sales' => 'Statistic Sales',
            'sort_order' => 'Sort Order',
            'product_id' => 'Product ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return DigisellerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DigisellerQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $productDigisellerId = $this->prepareProductDigisellerId($this->product_buy_url);

            $result = DigisellerApi::getProductInfo($productDigisellerId);
            
            $this->product_id_digiseller = $result->product->id;
            $this->name = $result->product->name;
            $this->in_stock = $result->product->in_stock;
            $this->price_wmr = $result->product->prices->wmr;
            $this->statistic_sales = $result->product->statistics->sales;
            $this->seller_id_digiseller = $result->product->seller->id;
            $this->seller_name_digiseller = $result->product->seller->name;

            return true;
        }
        return false;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        $digisellerId = $this->id;
        $productDigisellerId = $this->product_id_digiseller;
        $productSellerId = $this->seller_id_digiseller;
        
        /*
         * После добавлении Digiseller, добавляем отзывы
         * @param $insert boolean
         */
        if($insert) {
            $model = new Review();
            $model->setProductReviews($digisellerId, $productDigisellerId, $productSellerId);
        }
    }
    
    /*
     * Удаляем отзывы товара, после удаления Digiseller
     */
    public function afterDelete()
     {
         parent::afterDelete();
        
         Review::deleteAll(['digiseller_id' => $this->id]);

     }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('CURRENT_TIMESTAMP()'),
            ]
        ];
    }

    private function prepareProductDigisellerId($productBuyUrl)
    {
        $url = parse_url($productBuyUrl, PHP_URL_QUERY); // id_d=1512999&ai=361502
        parse_str($url, $params); // array(2) { ["id_d"]=> string(7) "1512999" ["ai"]=> string(6) "361502" }
    
        return ArrayHelper::getValue($params, 'id_d');
    }

}
