<?php

namespace app\modules\digiseller\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\digiseller\models\Digiseller]].
 *
 * @see \app\modules\digiseller\models\Digiseller
 */
class DigisellerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\digiseller\models\Digiseller[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\digiseller\models\Digiseller|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
