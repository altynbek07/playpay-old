<?php

namespace app\modules\digiseller\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\digiseller\models\Review]].
 *
 * @see \app\modules\digiseller\models\Review
 */
class ReviewQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\digiseller\models\Review[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\digiseller\models\Review|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
