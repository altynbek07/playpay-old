<?php

namespace app\modules\digiseller\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\digiseller\models\Digiseller;

/**
 * DigisellerSearch represents the model behind the search form of `app\modules\digiseller\models\Digiseller`.
 */
class DigisellerSearch extends Digiseller
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id_digiseller', 'seller_id_digiseller', 'in_stock', 'price_wmr', 'statistic_sales', 'sort_order', 'product_id'], 'integer'],
            [['product_url', 'product_buy_url', 'name', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Digiseller::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_id_digiseller' => $this->product_id_digiseller,
            'seller_id_digiseller' => $this->seller_id_digiseller,
            'in_stock' => $this->in_stock,
            'price_wmr' => $this->price_wmr,
            'statistic_sales' => $this->statistic_sales,
            'sort_order' => $this->sort_order,
            'product_id' => $this->product_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'product_url', $this->product_url])
            ->andFilterWhere(['like', 'product_buy_url', $this->product_buy_url])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
