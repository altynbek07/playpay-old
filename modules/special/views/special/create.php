<?php

/* @var $this yii\web\View */
/* @var $model app\modules\special\models\Special */

$this->title = 'Create Special';
$this->params['breadcrumbs'][] = ['label' => 'Specials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

