<?php

use yii\bootstrap\Html;
use naffiq\bridge\widgets\ActiveForm;
use kartik\select2\Select2;
use app\modules\product\models\Product;


/* @var $this yii\web\View */
/* @var $model app\modules\special\models\Special */
/* @var $form naffiq\bridge\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

        <?= $form->field($model, 'product_id')->relationalDropDown(Product::className(), 'id', 'name', ['options' => ['placeholder' => 'Select product']]) ?>

        <?= $form->field($model, 'discount_type')->widget(Select2::classname(), [
            'data' => $model->discountTypes,
            'options' => ['placeholder' => 'Select discount type'],
        ]); ?>

        <?= $form->field($model, 'discount')->textInput() ?>

        <?= $form->field($model, 'is_active')->switchInput() ?>

        <?= $form->field($model, 'start_date')->datePicker() ?>

        <?= $form->field($model, 'end_date')->datePicker() ?>

    </div>

    <div class="col-md-4">

    </div>
</div>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
