<?php

namespace app\modules\special\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use app\modules\product\models\Product;

/**
 * This is the model class for table "special".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $discount
 * @property integer $discount_type
 * @property integer $is_active
 * @property string $start_date
 * @property string $end_date
 * @property string $created_at
 * @property string $updated_at
 */
class Special extends \yii\db\ActiveRecord
{
    const DISCOUNT_TYPE_PERCENT = 0;
    const DISCOUNT_TYPE_PRICE = 1;

    public $discountTypes = [
        self::DISCOUNT_TYPE_PERCENT => 'Percent',
        self::DISCOUNT_TYPE_PRICE => 'Depreciated price'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'special';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'discount', 'discount_type', 'is_active'], 'integer'],
            [['start_date', 'end_date', 'created_at', 'updated_at'], 'safe'],
            [['product_id', 'discount', 'discount_type'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'discount' => 'Discount',
            'discount_type' => 'Discount Type',
            'is_active' => 'Is Active',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return \app\modules\special\models\query\SpecialQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\special\models\query\SpecialQuery(get_called_class());
    }

    /*
     * Применяем скидку на товар
     *
     * @param $oldPrice integer
     * @param $specialDiscount integer
     * @param $specialDiscountType integer
     * @return integer
     */
    public static function applyDiscount($oldPrice, $specialDiscount, $specialDiscountType)
    {
        if ($specialDiscountType === Special::DISCOUNT_TYPE_PERCENT) {
            $newPrice = $oldPrice - (($oldPrice * $specialDiscount) / 100);
            return $newPrice;
        } elseif ($specialDiscountType === Special::DISCOUNT_TYPE_PRICE) {
            $newPrice = $oldPrice - $specialDiscount;
            return $newPrice;
        }
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('CURRENT_TIMESTAMP()'),
            ]
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $product = Product::findOne($this->product_id);
        $product->is_special = $this->is_active;
        $product->save();
    }
}
