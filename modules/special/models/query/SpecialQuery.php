<?php

namespace app\modules\special\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\special\models\Special]].
 *
 * @see \app\modules\special\models\Special
 */
class SpecialQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\special\models\Special[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\special\models\Special|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
