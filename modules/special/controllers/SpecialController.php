<?php

namespace app\modules\special\controllers;

use naffiq\bridge\controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * SpecialController implements the CRUD actions for [[app\modules\special\models\Special]] model.
 * @see app\modules\special\models\Special
 */
class SpecialController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\special\models\Special';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\special\models\search\SpecialSearch';




    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::className(),
                    'modelClass' => 'app\modules\special\models\Special',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::className(),
                ],
            ]
        );
    }
}
